# FoxyProxy and Legacy Windows Applications

This file shows basic steps for integraton of FoxyProxy with legacy Windows 
applications. The integration uses the [APDUPlay](https://github.com/crocs-muni/APDUPlay)
project, which enhances the Windows Smartcard subsystem to be able to access 
remote smartcards via FoxyProxy.

## External Dependencies

* Windows OS - tested on Windows 7 and Windows 10.
* APDUPlay
* CloudFoxy - optional, FoxyProxy also supports traditional USB smartcards, which it accesses with GPPro 

## Install

The first step is to install the APDUPlay DLL. The 
[APDUPlay](https://github.com/crocs-muni/APDUPlay) project describes that in detail so just a few words to explain the goal.

APDUPlay is a "shim" library, which sits between Windows applications and Windows
smart-card subsystem. It can log the commands but also provide access to remote
smartcards via FoxyProxy.

There are two basic installation options, which depend on whether your legacy
application checks the system directory only for DLLs, or whether it also checks
its local directory.

When you have admin access to the computer where you want to use FoxyProxy, the
safe approach is to install APDUPlay into a Windows system folder. It can be either
System32, or SysWow64 - depending on the legacy application internal 
compatibility (64b or 32b).

### Windows Compatibility

We have come across issues with some version of Windows WinsCard.DLL. The version 
of the Windows DLL - WinsCard.DLL - matters and some new versions may not 
correctly work with Foxy.

If you follow the steps below without success, do get in touch at support@keychest.net.
We can provide help to identify a good version of the DLL.

### FoxyProxy Installation

You need to first install FoxyProxy. We recommand to do that on the Windows computer
where you use your legacy applications.

However, it is not necessary and FoxyProxy can be in a different geo location. For
security reasons, you should use a VPN or other network encryption to protect communication
between FoxyProxy and your Windows computer!

### APDU Install Example

This example if for legacy applications that use 32bit version of the DLL.

Download AP1. DU play from https://github.com/crocs-muni/APDUPlay - we need the 32bit version - 2.1.2 or later.
* Prepare the Windows system folder \Windows\SysWoW64 as follows
    1. open a PowerShell window
    2. cd \Windows\SysWow64
    3. takeown /f WinSCard.dll
    4. cacls.exe WinSCard.dll /G <yourname>:F   # alternatively you can use 'icacls.exe'
    5. rename WinSCard.dll WinSCard.dll_
* Create a folder "c:\FoxyProxy" - it can be any suitable location
* Open Windows System Settings where you can edit System Environmental Variables and add:
    1.  APDUPLAY = c:\FoxyProxy
* Copy files to enable the APDUPlay ()
    1. copy WinSCard.dll_ c:\windows\SysWow64\original32.dll
    2. "unzipped APDUPlay"\winscard_rules.txt - into the folder c:\FoxyProxy
    3. "unzipped APDUPlay"\winscard.dll - into the \Windows\sysWoW64\WinSCard.dll
* Edit the configuration file in C:\FoxyProxy\winscard_rules.txt
```
[WINSCARD]
LOG_EXCHANGED_APDU = 0
MODIFY_APDU_BY_RULES = 0
LOG_FUNCTIONS_CALLS = 0
AUTO_REQUEST_DATA = 0
LOG_BASE_PATH = c:\FoxyProxy

[REMOTE]
REDIRECT = 1
IP = 127.0.0.1
PORT = 4001
NEW_SOCKET_PER_COMMAND=1
```

* Cerify all is good by accessing smartcard(s) with your legacy application. You can see logs in the c:\FoxyProxy folder
    1. mismatch 32b / 64b will be seen in the log files

* Once all works, add a new env. variable APDUPLAY_DISABLE_LOGGING, this will increase the speed of smartcard communication
